<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Insérer automatiquement l'appel au js dans les formulaires
 * @param array $flux
 * @return array $flux modifié
 **/
function saisie_calcul_formulaire_fond($flux) {
	static $flag;
	if (!$flag && stripos($flux['data'], 'saisie_calcul') !== false) {
		include_spip('inc/filtres');
		$flux['data'] .= "<script src='" . timestamp(find_in_path('javascript/saisie_calcul.js')) . "'></script>";
		$flag = true;
	}
	return $flux;
}

/**
 * Si jamais les js d'afficher_si ne sont pas insérés au niveau du plugin saisies,
 * on les insère
**/
function saisie_calcul_affichage_final(string $flux): string {
	if (
		stripos($flux, 'saisie_calcul') !== false
		&& stripos($flux, 'saisies_afficher_si_js') === false
	) {
		include_spip('inc/filtres');
		$js = '<script src=\'' . timestamp(find_in_path('javascript/saisies_afficher_si.js')) . '\'></script>';
		$flux = str_replace('</head>', $js . '</head>', $flux);
	}
	return $flux;
}

/*
 * La saisie calcul est autonome
 * @param array $flux
 * @return $flux
*/
function saisie_calcul_saisies_autonomes(array $flux): array {
	$flux[] = 'calcul';
	return $flux;
}
