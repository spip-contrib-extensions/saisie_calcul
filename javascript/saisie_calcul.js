$(function(){
	function calcul_arrondi(nb, precision = 0) {
		nb = nb * Math.pow(10, precision);
		nb = Math.round(nb);
		nb = nb / Math.pow(10, precision);
		return nb;
	}
	// Function permettant de récuperer la valeur d'une saisie, quelque soit la nature de la saisie,
	// Transforme en nombre + met 0 si jamais vide
	function get_valeur_saisie_pour_calcul(champ, coche = false) {
		if (
			$('.editer_'+champ).hasClass('afficher_si_masque')
			|| $('.editer_'+champ ).parents('.afficher_si_masque').length > 0
		) {
			return 0;
		}
		var valeur_champ = '';
		if (afficher_si_current_data.hasOwnProperty(champ)) {
			valeur_champ = afficher_si_current_data[champ];
		}
		if (coche) {
			valeur_champ = valeur_champ.length;
		} else if (Array.isArray(valeur_champ)) {
			valeur_champ = valeur_champ.reduce((a, b) => Number(a) + Number(b), 0);
		}
		if (!valeur_champ) {
			valeur_champ = 0;
		}
		valeur_champ = Number(valeur_champ);
		return valeur_champ;
	}

	function actualiser_calculs() {
		$("form .saisie_calcul input[data-calcul]").each(function(){
			var expr = $(this).attr("data-calcul");
			expr = JSON.parse(expr);
			expr = expr['calcul'];
			avant = $(this).val();
			var resultat = eval(expr);
			if (avant != resultat) {
				$(this).val(resultat);
				if (!(isNaN(resultat) && avant == 'NaN')) {
					$(this).trigger('change');
				}
			}
		})
	}

	// Recuperer tous les champs qui actualisent les calculs
	champs_generateurs = [];
	$("form [data-calcul]").each(function() {
		var expr = $(this).attr("data-calcul");
		expr = JSON.parse(expr);
		champs_generateurs = champs_generateurs.concat(expr['dependances']);
	});
	champs_generateurs = [... new Set(champs_generateurs)];//Eviter les doublons
	for (c in champs_generateurs) {
		champ = champs_generateurs[c];
		$('[name=' + champ + '],  [name=' + champ + '_choix_alternatif]').on('input', function() {
			afficher_si_set_current_data($(this).parents('form'));
			actualiser_calculs();
		});
	}
	// Code historique, on le garde pour les éventuels trigger
	$("form").has('[data-calcul]').change(function(){
		afficher_si_set_current_data($(this));
		actualiser_calculs();
	});
	actualiser_calculs();//Actualiser au chargement de la page, au cas où l'on serait en multietape
});
