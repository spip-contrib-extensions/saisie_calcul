<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/saisie_calcul.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'option_calcul_label' => 'Calcul à effectuer',
	'option_type_hidden_label' => 'Champ invisible',
	'option_type_input_label' => 'Champ visible',
	'option_type_label' => 'Type de champ',

	// S
	'saisie_calcul_titre' => 'Résultat d’un calcul'
);
