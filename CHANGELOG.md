# Changelog
## 3.4.0 - 2024-16-01

### Added

- Possibilité d'utiliser `@checkbox@:COCHE` pour compter le nombre de case cochées sur une saisie

## 3.2.4 - 2024-03-11

### Fixed

- #17 Actualiser en temps réel la valeur à partir d'un choix alternatif

## 3.2.3 - 2024-01-19

### Fixed

- #12 Ne mettre qu'une seule fois l'écouteur sur le formulaire, et uniquement s'il a des saisies `calcul` dedans
- #10 Tenir compte du fait qu'une saisie peut être masquée par `afficher_si` parce que l'un de ses parents est masqué

## 3.2.2 - 2024-01-10

### Fixed

- Eviter des erreurs fatales dans certains contexte d'appel

## 3.2.1 - 2023-07-19

### Fixed

- #8 Ne pas transformer les nombres en entier au niveau de PHP
## 3.2.0 - 2023-07-15

### Changed

- #6 Les calculs se font à l'input des saisies sources en plus du change du formulaire.

## 3.1.1 - 2023-02-27

### Fixed

- Compatibilité SPIP 4.2

## 3.1.0 - 2022-12-14

### Added

- Possibilité de dire qu'un calcul est masqué (pour faire des calculs intermédiaires)

### Changed

- Constructeur de saisie : mettre l'affichage conditionnel dans un onglet à part, comme pour les autres saisies
