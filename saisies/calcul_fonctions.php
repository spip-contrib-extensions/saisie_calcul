<?php

/**
 * Transforme une expression de calcul entrée par l'utilisateur/trice en expression interprétable en JS
 * @param string $expr
 * @param array $erreurs les éventuelles erreurs du formulaire
 * @return string json comprenant d'une part le calcul, d'autre part les dependances immédiates
**/
function saisie_calcul_2_js($expr, $erreurs = []) {
	include_spip('inc/saisies');
	$expr = saisie_calcul_securiser($expr);
	preg_match_all('#@(.*)@(:COCHE)*?#U', $expr, $matches, PREG_SET_ORDER);
	if (!$matches) {
		return '';
	}
	$saisies_masquees = array_keys(saisies_lister_par_nom(saisies_afficher_si_liste_masquees('get')));//Retrouver les saisies masquées par afficher_si, car elle ne sont vidées qu'à la toute fin du formulaire (je ne sais plus pourquoi du reste, mais il y avait sans doute une très bonne raison)
	unset($erreurs['_etapes']);
	$erreurs = array_filter($erreurs);
	foreach ($matches as $m) {
		$full_expr = $m[0];
		$champ = $m[1];
		$coche = $m[2] ?? false;
		if (in_array($champ, $saisies_masquees)) {
			$expr = str_replace("@$champ@", '0', $expr);
		} elseif (
			(($valeur = saisies_request($champ)))// Si on possède une valeur dans le request, c'est qu'on est sur une étape suivante d'un formulaire multiétape...
			&& !$erreurs//sauf si on est réaffiche le formulaire actuel pour cause d'erreur
		) {
			if ($coche) {
				$valeur = count($valeur);
			} elseif (is_array($valeur)) {
				$valeur = array_sum($valeur);
			}
			$expr = str_replace("@$champ@", $valeur, $expr);
		} else {
			if ($coche) {
				$expr = str_replace($full_expr, "get_valeur_saisie_pour_calcul('$champ', true)", $expr);
			} else {
				$expr = str_replace($full_expr, "get_valeur_saisie_pour_calcul('$champ')", $expr);
			}
		}
	}
	$expr = str_replace('ROUND', 'calcul_arrondi', $expr);
	$expr = str_replace("\n", '', $expr);
	$expr = str_replace("\r", '', $expr);
	$dependances = array_column($matches, 1);
	return str_replace('"', '&quot;', json_encode(['calcul' => $expr, 'dependances' => $dependances]));
}


/**
 * Transforme une expression de calcul entrée par l'utilisateur/trice en expression interprétable en PHP
 * @param string $expr
 * @return string
**/
function saisie_calcul_2_php($expr) {
	$expr = saisie_calcul_securiser($expr);
	$expr = preg_replace('#@(.*)@(:COCHE)*?#U', 'floatval(saisie_calcul_request(\'$1\', boolval(\'$2\')))', $expr);
	$expr = str_replace('ROUND', 'saisie_calcul_arrondi', $expr);
	return $expr;
}
/**
 * Récupère une valeur en _request
 * Si c'est un nombre, retourne directement
 * Si c'est un tableau, fait la somme
 * @param string $champ
 * @param bool $coche
 * @return float
**/
function saisie_calcul_request(string $champ, bool $coche = false): float{
	include_spip('inc/saisies');
	$valeur = saisies_request($champ);
	if (is_array($valeur)) {
		if ($coche) {
			return count($valeur);
		} else {
			return array_sum($valeur);
		}
	} else {
		return floatval($valeur);
	}
}
/**
 * Une fonction d'arrondi php qui imite la fonction d'arrondi JS
 * @param float|int $valeur
 * @param int $precision
 * @return float
**/
function saisie_calcul_arrondi($valeur, $precision = 0) {
	if ($valeur > 0) {
		return round($valeur, $precision, PHP_ROUND_HALF_UP);
	} else {
		return round($valeur, $precision, PHP_ROUND_HALF_DOWN);
	}
}

/**
 * Sécurise une expression de calcul passée en paramètre.
 * N'autorise que :
 * - texte entre @@
 * - opérateurs des opérations de base (+*-/)
 * - parenthèses
 * - point, virgule
 * - nombre
 * - ROUND (pour l'arrondi)
 * - COCHE
 * @param string $expr
 * @return string $expr soit l'expression, soit rien si jamais cela ne respect pas les règles
**/
function saisie_calcul_securiser($expr) {
	if (!$expr) {
		return '';
	}
	$hors_arobase = '#('
		. '\d|'
		. '\(|\)|'
		. '\+|'
		. '\*|'
		. '\/|'
		. '-|'
		. '\.|'
		. '\,|'
		. 'ROUND|'
		. ':COCHE'
		. ')#';
	$arobase = '#@.*@#U';
	$valable = preg_replace($hors_arobase, '', $expr);
	$valable = preg_replace($arobase, '', $valable);
	if (!trim($valable)) {//Si à la fin il ne reste plus rien, c'est que c'est bon, on retourne donc l'expression
		return $expr;
	} else {//Sinon c'est le mal, et on retourne le vide
		return '';
	}
}
